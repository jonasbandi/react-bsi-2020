import React, { useState, useEffect } from 'react';
import produce from 'immer';
import { loadToDos, storeToDos } from './persistence';
import { NewTodos } from './NewTodo';
import { TodoList } from './TodoList';

function App() {
  console.log('Render');

  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const todos = loadToDos();
    setTodos(todos);
  }, []);

  function addToDo(newTodo) {
    const newToDos = produce(todos, todosDraft => {
      todosDraft.push(newTodo);
    });
    setTodos(newToDos);
    storeToDos(newToDos);
  }

  const removeToDo = toDo => {
    const newToDos = produce(todos, todosDraft => {
      todosDraft.splice(todos.indexOf(toDo), 1);
    });
    setTodos(newToDos);
    storeToDos(newToDos);
  };

  return (
    <div className="App">
      <div className="todoapp-header">
        <h1 id="title">Simplistic ToDo</h1>
        <h4>A most simplistic ToDo List in React.</h4>
      </div>

      <section className="todoapp">
        <NewTodos onAddTodo={addToDo} />

        <div className="main">
          <TodoList todos={todos} onRemoveTodo={removeToDo} />
        </div>
      </section>
      <footer className="info">
        <p>
          JavaScript Example / Initial template from <a href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
        </p>
      </footer>
    </div>
  );
}

export default App;
