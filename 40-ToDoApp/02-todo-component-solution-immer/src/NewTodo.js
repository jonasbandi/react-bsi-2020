import React, { useState } from 'react';

export function NewTodos(props) {
  const [toDoTitle, setToDoTitle] = useState('');

  const inputChange = e => {
    setToDoTitle(e.target.value);
  };

  function addToDo(e) {
    e.preventDefault();
    const newTodo = { id: Math.random(), title: toDoTitle, completed: false };
    props.onAddTodo(newTodo);
    setToDoTitle('');
  }

  let submitButton = null;
  if (toDoTitle.length > 3) {
    submitButton = (
      <button id="add-button" className="add-button">
        +
      </button>
    );
  }

  return (
    <form className="new-todo" onSubmit={addToDo}>
      <input
        id="todo-text"
        name="toDoTitle"
        type="text"
        placeholder="What needs to be done?"
        autoFocus
        autoComplete="off"
        value={toDoTitle}
        onChange={inputChange}
      />
      {submitButton}
    </form>
  );
}
