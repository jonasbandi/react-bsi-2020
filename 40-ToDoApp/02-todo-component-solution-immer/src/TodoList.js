import React from 'react';

export function TodoList({ todos, onRemoveTodo }) {
  return (
    <ul id="todo-list" className="todo-list">
      {todos.map(t => (
        <li key={t.id}>
          {t.title}
          <button onClick={() => onRemoveTodo(t)}>X</button>
        </li>
      ))}
    </ul>
  );
}
