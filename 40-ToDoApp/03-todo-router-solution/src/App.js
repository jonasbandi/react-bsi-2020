import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, NavLink, Route, Switch } from 'react-router-dom';
// import ToDoScreen from './components/ToDoScreen';
// import DoneScreen from './components/DoneScreen';
// import DetailScreen from './components/DetailScreen';

const ToDoScreen = lazy(() => import('./components/ToDoScreen'));
const DoneScreen = lazy(() => import('./components/DoneScreen'));
const DetailScreen = lazy(() => import('./components/DetailScreen'));

function App() {
  return (
    <div className="App">
      <div className="todoapp-header">
        <h1 id="title">Simplistic ToDo</h1>
        <h4>A most simplistic ToDo List in React.</h4>
      </div>

      <section className="todoapp">
        <Router>
          <div>
            <div className="nav">
              <NavLink exact to="/" activeClassName="selected">
                Pending
              </NavLink>
              <NavLink exact to="/done" activeClassName="selected">
                Done
              </NavLink>
            </div>
            <Suspense fallback={<div>Loading ...</div>}>
              <Switch>
                <Route path="/detail/:id">
                  <DetailScreen/>
                </Route>
                <Route path="/done">
                  <DoneScreen/>
                </Route>
                <Route path="/">
                  <ToDoScreen/>
                </Route>
              </Switch>
            </Suspense>
          </div>
        </Router>
      </section>
      <footer className="info">
        <p>
          JavaScript Example / Initial template from{' '}
          <a href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
        </p>
      </footer>
    </div>
  );
}

export default App;
