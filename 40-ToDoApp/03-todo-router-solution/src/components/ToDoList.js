import React from 'react';
import {PropTypes} from 'prop-types';
import ToDoListItem from './ToDoListItem';

ToDoList.propTypes = {
  todos: PropTypes.array.isRequired,
  onRemoveToDo: PropTypes.func.isRequired
};

function ToDoList({todos, onRemoveToDo}) {
    return (
    <ul id="todo-list" className="todo-list">
      {
        todos.map(t => (
          <ToDoListItem key={t.id} todo={t} onRemoveToDo={onRemoveToDo}/>
        ))
      }
    </ul>
  );
}

export default ToDoList;
