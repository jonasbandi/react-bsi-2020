import React, {useState, useEffect} from 'react';
import ToDoList from './ToDoList';
import {loadToDos, storeToDos} from '../persistence';

function DoneScreen() {

  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const todos = loadToDos();
    updateToDos(todos);
  }, []);

  function removeToDo(toDo) {
    const todos = loadToDos();
    const newToDos = todos.filter(t => t.id !== toDo.id);
    storeToDos(newToDos);

    updateToDos(newToDos);
  }

  function updateToDos(updatedToDos) {
    const pendingToDos = updatedToDos.filter(t => t.completed)
    setTodos(pendingToDos);
  }

  return (
    <div>
      <div className="main">
        <ToDoList todos={todos} onRemoveToDo={removeToDo}/>
      </div>
    </div>
  );

}

export default DoneScreen;
