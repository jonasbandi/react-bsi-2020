const {useContext, useEffect, useReducer} = React;

// This is a basic implementation of the pattern.
// For a better implementation see:
// https://kentcdodds.com/blog/how-to-use-react-context-effectively

//////////////////////
// Application Logic
////////////////////
const initialState = {count: 0, updated: new Date(), message: 'Great demo!'};

function counterReducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {...state, count: state.count + 1, updated: new Date()};
    case 'reset':
      return {...state, count: 0, updated: new Date()};
    default:
      return state;
  }
}

//////////////////////
// Components
////////////////////
const CountContext = React.createContext();
function App() {

  const store = useReducer(counterReducer, initialState);

  useEffect(() => {
    const [state, dispatch] = store;
    setInterval(() => dispatch({type: 'increment'}), 1000);
  }, []);

  return (
    <CountContext.Provider value={store}>
      <CountScreen/>
    </CountContext.Provider>
  )
}

function CountScreen() {
  return (
    <div>
      <CountCard/>
    </div>
  )
}

function CountCard() {
  const [state, dispatch] = useContext(CountContext);

  return (
    <div>
      <CountDisplay/>

      <div>
        <button onClick={() => dispatch({type: 'reset'})}>Reset</button>
      </div>
    </div>
  )
}

function CountDisplay() {
  const [state] = useContext(CountContext);

  return (
    <div>
      Time: {state.count}
    </div>
  )
}

ReactDOM.render(<App/>, document.getElementById('root'));


