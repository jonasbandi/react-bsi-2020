function WrapperComponent({render}) {
  return (
    <div>
      <hr/>
      <div className="container">
        {render()}
      </div>
      <hr/>
    </div>
  )
}

function Content() {
  return (
    <div>Test!</div>
  )
}

function App() {
  return (
    <WrapperComponent render={() => {
      return <Content/>
    }}/>
  );
}

ReactDOM.render(<App/>, document.getElementById("root"));

