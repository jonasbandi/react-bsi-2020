const { useState, useEffect, useRef } = React;

function GreeterComponent() {
  const registeredButtonsRef = useRef([]);

  function registerButton(button) {
    registeredButtonsRef.current.push(button);
  }

  useEffect(() => {
    registeredButtonsRef.current[2].focus();
  }, []);

  return (
    <div>
      <h1>React works with a virtual DOM</h1>
      <button ref={domElement => registerButton(domElement)}>Button 1</button>
      <br />
      <button ref={domElement => registerButton(domElement)}>Button 2</button>
      <br />
      <button ref={domElement => registerButton(domElement)}>Button 3</button>
    </div>
  );
}

ReactDOM.render(<GreeterComponent />, document.getElementById('root'));

// DEMO:
// - create ref to title, apply jquery-fittext plugin:
// $(this.title).fitText(1.5)
