function WrapperComponent({ children }) {
  return (
    <div className="container">
      <hr />
      {children}
      <hr />
    </div>
  );
}

class App extends React.Component {
  render() {
    return (
      <WrapperComponent>
        <h1>Gugus</h1>
      </WrapperComponent>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
