const {HashRouter: Router, Route, Link, Switch, useParams, useLocation, useRouteMatch, useHistory } = ReactRouterDOM;

function Home() {
  const history = useHistory();
  return (
    <div>
      <h2>Home</h2>
      <button onClick={() => history.push('/about/1')}>Go</button>
    </div>
  );
}

function About() {
  const params = useParams();
  const location = useLocation();
  const match = useRouteMatch();
  const history = useHistory();
  return (
    <div>
      <h2>About</h2>
      <div>Routing Information</div>
      <pre>{JSON.stringify(params)}</pre>
      <pre>{JSON.stringify(location)}</pre>
      <pre>{JSON.stringify(match)}</pre>
      <button onClick={() => history.push('/')}>Go Home</button>
    </div>
  );
}

function App() {

  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about/42">About</Link>
          </li>
        </ul>

        <hr/>

        <Switch>
          <Route path="/about/:id"> <About/> </Route>
          <Route path="/"> <Home/> </Route>
        </Switch>
      </div>
    </Router>
  );
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);


